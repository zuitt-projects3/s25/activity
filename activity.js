
db.fruits.aggregate([
  {$match: {supplier: "Red Farms Inc"}},
  {$count: "fruitsOfRedFarms"}
])

db.fruits.aggregate([
  {$match: {supplier: "Green Farming and Canning"}},
  {$count: "fruitsOfGreenFarm"}
])

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"$supplier", avg_price: {$avg: "$price"}}}
])

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"$supplier", max_price: {$max: "$price"}}}
])

db.fruits.aggregate([
  {$match: {onSale:true}},
  {$group: {_id:"$supplier", min_price: {$min: "$price"}}}
])
